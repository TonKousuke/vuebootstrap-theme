Vue.component('header-menu', require("./components/HeaderMenu").default);

Vue.component('home-recent-blogs', require("./components/HomeRecentBlogs").default);
Vue.component('home-recent-news', require("./components/HomeRecentNews").default);
Vue.component('home-recent-promotions', require("./components/HomeRecentPromotions").default);

Vue.component('content-post-item', require("./components/ContentPostItem").default);

// Vue.component('recent-blogs', require("./components/RecentBlogs").default);
// Vue.component('recent-news', require("./components/RecentNews").default);
// Vue.component('recent-promotions', require("./components/RecentPromotions").default);