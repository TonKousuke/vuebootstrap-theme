@extends('layouts.master')

@section('content')
<div class="container">

    <home-recent-blogs></home-recent-blogs>

    <hr>

    <home-recent-news></home-recent-news>

    <hr>

    <home-recent-promotions></home-recent-promotions>

</div>
@stop

@push('css-stack')
@endpush

@push('js-stack')
@endpush